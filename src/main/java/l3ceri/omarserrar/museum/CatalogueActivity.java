package l3ceri.omarserrar.museum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class CatalogueActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static CatalogueActivity current = null;
    private ArrayList<ObjetMusee> objets = null;
    private RecyclerView catalogueListe;
    private String categorie;
    private  String query;
    private int sortType = 3;
    private Spinner sortMethod;
    private LinearLayout chargement;
    private SwipeRefreshLayout refreshLayout;
    public static MuseeDBHelper dbHelper;
    private static final String[] sortMethodArray = {"Date de fabrication (Croissant)", "Date de fabrication (Decroissant)", "Ordre alphabetique(Croissant)", "Ordre alphabetique(Decroissant)",""};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catalogue_view);
        dbHelper = new MuseeDBHelper(this);
        chargement = findViewById(R.id.chargementCatalogue);
        refreshLayout = findViewById(R.id.actualiser);
        refreshLayout.setOnRefreshListener(this);

        sortMethod = findViewById(R.id.sortMethod);
        sortMethod.setAdapter(new ArrayAdapter<String>(CatalogueActivity.this,
                android.R.layout.simple_spinner_item,sortMethodArray));
        sortMethod.setSelection(4);
        sortMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortType = position;
                sort(sortType);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        categorie = getIntent().getStringExtra("CATEGORY");
        query = getIntent().getStringExtra("query");
        catalogueListe = findViewById(R.id.catalogueListe);
        catalogueListe.setLayoutManager(new LinearLayoutManager(this));
        catalogueListe.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));


        actualiser();
        if(MuseeDBHelper.objets.size()==0){
            new CeriMuseumAPI(this).execute();
            chargement.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        new CeriMuseumAPI(CatalogueActivity.this).execute();
        chargement.setVisibility(View.VISIBLE);
    }
    public void actualiser(){
        if(query!=null && !query.isEmpty()){
            setTitle("Recherche: "+query);
            search(query);
        }
        else{
            if(categorie!=null && !categorie.isEmpty())
                setTitle("Categorie: "+categorie);
            else
                setTitle("Catalogue");
            afficherCatalogue(MuseeDBHelper.categories.get(categorie));
        }
        sort(sortType);
        refreshLayout.setRefreshing(false);
        chargement.setVisibility(View.GONE);
    }
    public void sort(int type){
        switch (type) {
            case 0:
                sortList(false, true);
                break;
            case 1:
                sortList(false, false);
                break;
            case 2:
                sortList(true, true);
                break;
            case 3:
                sortList(true, false);
                break;
        }
    }
    public void search(String query){
        System.out.println("Searching Objets: "+query+" ....");
        ArrayList<ObjetMusee> resultats = new ArrayList<>();
        for(ObjetMusee objetMusee: MuseeDBHelper.objets.values()){
            if(objetMusee.containtValue(query))
                resultats.add(objetMusee);
        }
        objets = new ArrayList<ObjetMusee>(resultats);
        catalogueListe.setAdapter(new CatalogueAdapter(objets));
    }
    public void afficherCatalogue(Categorie categorie){
        if(categorie==null){
            objets = new ArrayList<ObjetMusee>(new ArrayList<ObjetMusee>(MuseeDBHelper.objets.values()));
            catalogueListe.setAdapter(new CatalogueAdapter(new ArrayList<ObjetMusee>(MuseeDBHelper.objets.values())));
        }

        else{
            objets = new ArrayList<ObjetMusee>(categorie.getAllObjet());
            catalogueListe.setAdapter(new CatalogueAdapter(new ArrayList<ObjetMusee>(categorie.getAllObjet())));
        }

    }
    public void sortList(boolean alpha, boolean asc){
        ArrayList<ObjetMusee> objetMusees = sortData(alpha,asc);
        if(!alpha){
            int size = objetMusees.size();
            int lastYear = -5;

            for(int i=0;i<size;i++){
                if( objetMusees.get(i)!=null && objetMusees.get(i).getYear() != lastYear){
                    lastYear = objetMusees.get(i).getYear();
                    objetMusees.add(i,null);

                    size++;
                }
            }
        }

        catalogueListe.setAdapter(new CatalogueAdapter(objetMusees));
    }
    public ArrayList<ObjetMusee> sortData(boolean alpha, boolean asc){
        final boolean alphaSort = alpha;
        final boolean ascSort = asc;
        ArrayList<ObjetMusee> objetList = (ArrayList<ObjetMusee>) objets.clone();
        Collections.sort(objetList, new Comparator<ObjetMusee>() {
            @Override
            public int compare(ObjetMusee objet1, ObjetMusee objet2)
            {
                if(alphaSort)
                    if(ascSort)
                        return objet1.getName().compareTo(objet2.getName());
                    else
                        return objet2.getName().compareTo(objet1.getName());
                else{
                    if(ascSort)
                        return (objet1.getYear() > objet2.getYear())?1:-1;
                    else
                        return (objet1.getYear() < objet2.getYear())?1:-1;
                }
            }
        });

        return objetList;
    }
    public static CatalogueActivity getCatalogueActivity(){
        return current;
    }
    class CatalogueAdapter extends RecyclerView.Adapter<RowHolder> {
        private ArrayList<ObjetMusee> objetListes;
        CatalogueAdapter(ArrayList<ObjetMusee> objets){
            this.objetListes = objets;
        }
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if(viewType==0){
                return (new RowHolder(getLayoutInflater().inflate(R.layout.catalogue_element, parent, false)));
            }
            else{
                return (new RowHolder(getLayoutInflater().inflate(R.layout.year_row, parent, false)));
            }

        }
        @Override
        public int getItemViewType(int position) {
            if (objetListes.get(position) == null) {
                return 1;
            } else {
                return 0;
            }
        }
        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            try {
                if(objetListes.get(position)!=null){
                    holder.bindModel(objetListes.get(position));
                }
                else
                    holder.bindModelYear(objetListes.get(position+1).getYear());
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            System.out.println("Sizzze "+objetListes.size());
            return objetListes.size();
        }
    }
}
class RowHolder  extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    TextView objetName =null;
    TextView year = null;
    TextView objetCategories =null;
    ImageView objetImage = null;
    ObjetMusee objet;
    RowHolder(View row) {
        super(row);
        year = (TextView) row.findViewById(R.id.yearRow);
        objetName=(TextView)row.findViewById(R.id.name);
        objetCategories=(TextView)row.findViewById(R.id.categories);
        objetImage = (ImageView)row.findViewById(R.id.objetImage);
        if(objetImage!=null)
          row.setOnClickListener(this);

    }

    @Override
    public void onClick(View v){
        Intent myIntent = new Intent(v.getContext(), ObjetActivity.class);
        myIntent.putExtra("objet", objet);
        ((CatalogueActivity)v.getContext()).startActivity(myIntent);
        //;
    }
    void bindModelYear(int year){
        this.year.setText(year==0?"Indéfinie":year+"");
    }
    void bindModel(ObjetMusee objetMusee) {

        if (objetMusee != null) {
            this.objet = objetMusee;
            objetName.setText(objet.getName());
            String categories = "";
            for(Categorie categorie: objet.getCategories()){
                categories += categorie.getName()+", ";
            }
            objetCategories.setText(categories);
            objetImage.setImageDrawable(null);
            if(new DownloadImageTask(objetImage).execute(objet.getThumbNail())==null)
                objetImage.setImageResource(R.drawable.ic_launcher_background);
        }
    }
}

class DownloadImageTask extends AsyncTask<URL, Void, Bitmap> {
    ImageView badgeImageView;
    public static HashMap<URL, Bitmap> picturesCache = new HashMap<>();
    public DownloadImageTask(ImageView badgeImageView) {
        this.badgeImageView = badgeImageView;
    }

    protected Bitmap doInBackground(URL... url) {
        Bitmap badgeBitmap = null;
        if(picturesCache.containsKey(url[0])){
            return picturesCache.get(url[0]);
        }
        Bitmap photo = CatalogueActivity.dbHelper.getPic(url[0].toString());
        if(photo!=null){
            System.out.println("Recuperation de "+url[0]+" de la bd");
            return photo;
        }

        try {
            InputStream inputStream = url[0].openStream();
            badgeBitmap = BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        picturesCache.put(url[0], badgeBitmap);
        long success = CatalogueActivity.dbHelper.addPicture(badgeBitmap, url[0].toString(), Bitmap.CompressFormat.PNG);
        System.out.println(url[0].toString()+" "+success);
        return badgeBitmap;
    }
    protected void onPostExecute(Bitmap result) {
        if(result!=null)
            badgeImageView.setImageBitmap(result);
    }
}